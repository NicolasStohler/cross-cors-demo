import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // https://medium.com/codingthesmartway-com-blog/angular-4-3-httpclient-accessing-rest-web-services-with-angular-2305b8fd654b

  title = 'app';
  results = '';
  qod = null;

  directResultAD = null;
  directResultGet = null;
  directResult = null;
  forwardResult = null;

  constructor(private httpClient: HttpClient) {    
  }

  ngOnInit(): void {
    // this.httpClient.get('https://api.github.com/users/seeschweiler').subscribe(data => {
    //   console.log(data);
    // });

    this.httpClient.get('https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1').subscribe(data => {
      // console.log(data[0]);
      this.qod = data[0];
    });

    this.httpClient.get('http://localhost:57780/api/test').subscribe(data => {
      // console.log(data[0]);
      this.directResultAD = data;
    });

    this.httpClient.get('http://localhost:57859/api/values').subscribe(data => {
      // console.log(data[0]);
      this.directResultGet = data;
    });

    // this should be blocked!
    // let params = new HttpParams();
    // params = params.append('user', 'Nicolas');
    // params = params.append('password', 'wusel');

    var username = 'Nicolas';
    var password = 'wusel';
    var unencoded = `${username}:${password}`;

    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `Basic ${btoa(unencoded)}`);

    console.log("headers", headers);

    this.httpClient.get('http://localhost:57859/api/values/5', { 
        headers: headers,
        //params: params, 
        withCredentials: true
      }).subscribe(data => {
      // console.log(data[0]);
      this.directResult = data;
    });

    this.httpClient.get('http://localhost:57780/api/forward').subscribe(data => {
      // console.log(data[0]);
      this.forwardResult = data;
    });



    
  }
}
