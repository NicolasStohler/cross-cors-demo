﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Easy.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Data.Edm.Csdl;
using Newtonsoft.Json;

namespace AdServer.Controllers
{
    [Route("api")]
    [Produces("application/json")]
    public class ValuesController : Controller
    {
        private readonly IRestClient _restClient;

        public ValuesController(IRestClient restClient)
        {
            _restClient = restClient;
        }

        // GET api/test
        [HttpGet("test")]
        public IActionResult Get()
        {
            return Ok(new
            {
                hallo = "velo AD",
                wusel = "dusel AD"
            });
            //return new string[] { "value1", "value2" };
        }

        // GET api/forward
        [HttpGet("forward")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new Dictionary<string, string>();

            var username = "Nicolas";
            var password = "wusel";
            //query.Add("user", "Nicolas");
            //query.Add("password", "wusel");

            var authenticationHeader = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(
                    System.Text.Encoding.UTF8.GetBytes($"{username}:{password}")));

            var url = "http://localhost:57859/api/values/5";
            var finalUrl = QueryHelpers.AddQueryString(url, query);

            var message = new HttpRequestMessage(HttpMethod.Get, finalUrl)
            {
                // Content = new StringContent()
            };
            message.Headers.Authorization = authenticationHeader;
            

            var response = await _restClient.SendAsync(message);

            if (response.IsSuccessStatusCode)
            {
                var queryResult = await response.Content.ReadAsStringAsync();

                return Ok(new
                {
                    content = queryResult,
                    hallo = "velo AD",
                    wusel = "dusel AD"
                });
            }
            else
            {
                return Unauthorized();
            }
        }

        //public async Task<string> Login(string appLoginUrl, CredentialsViewModel credentials)
        //{
        //    // http://www.nimaara.com/2016/11/01/beware-of-the-net-httpclient/

        //    var jsonContent = JsonConvert.SerializeObject(credentials);

        //    var message = new HttpRequestMessage(HttpMethod.Post, appLoginUrl)
        //    {
        //        Content = new StringContent(jsonContent, Encoding.UTF8, "application/json")
        //    };

        //    var response = await _restClient.SendAsync(message);

        //    var queryResult = await response.Content.ReadAsStringAsync();

        //    // queryResult is already JSON!
        //    return queryResult;
        //}


    }
}
