﻿using DemoServer.Middleware;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace DemoServer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        // [EnableCors("AllowSpecificOrigin")]
        public IActionResult Get()
        {
            return Ok(new 
            {
                hallo = "velo", 
                wusel = "dusel"
            });
            //return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [MiddlewareFilter(typeof(BasicAuthorizationPipeline))]
        [EnableCors("AllowSpecificOrigin")]
        public IActionResult Get(int id
            // , string user, string password
            )
        {
            //if(user == null || password == null)
            //{
            //    return Unauthorized();
            //}
            //if(!(user.Equals("Nicolas") && password.Equals("wusel")))
            //{
            //    return Unauthorized();
            //    // return new StatusCodeResult(StatusCodes.Status401Unauthorized);
            //}

            // only the adAuthServer should be allowed to access this!
            return Ok(new
            {
                value = id,
                message = "only accessibly by ad server?"
            });
        }
    }
}
