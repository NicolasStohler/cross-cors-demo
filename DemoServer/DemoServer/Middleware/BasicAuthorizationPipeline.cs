﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace DemoServer.Middleware
{
    public class BasicAuthorizationPipeline
    {
        public void Configure(IApplicationBuilder app, IConfiguration configuration)
        {
            app.UseMiddleware<BasicAuthorizationMiddleware>(configuration["AdminWhiteList"]);
        }
    }

}
