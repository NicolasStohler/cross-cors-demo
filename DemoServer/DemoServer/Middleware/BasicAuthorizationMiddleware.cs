﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace DemoServer.Middleware
{
    public class BasicAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<BasicAuthorizationMiddleware> _logger;
        private readonly string _adminWhiteList;

        public BasicAuthorizationMiddleware(
            RequestDelegate next,
            ILogger<BasicAuthorizationMiddleware> logger
            , string adminWhiteList
            )
        {
            _adminWhiteList = adminWhiteList;
            //_adminWhiteList = "127.0.0.1;192.168.1.5";
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var basicAuthenticationHeader = context.Request.Headers["Authorization"]
                .FirstOrDefault(header => header.StartsWith("Basic", StringComparison.OrdinalIgnoreCase));
            var decodedHeader = new BasicAuthenticationHeaderValue(basicAuthenticationHeader);

            if (decodedHeader.UserIdentifier == "Nicolas" && decodedHeader.UserPassword == "wusel")
            {
                // continue processing
                await _next.Invoke(context);
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return;
            }

            //if (context.Request.Method != "GET")
            //{
            //    var remoteIp = context.Connection.RemoteIpAddress;
            //    _logger.LogInformation($"Request from Remote IP address: {remoteIp}");

            //    string[] ip = _adminWhiteList.Split(';');
            //    if (!ip.Any(option => option == remoteIp.ToString()))
            //    {
            //        _logger.LogInformation($"Forbidden Request from Remote IP address: {remoteIp}");
            //        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            //        return;
            //    }
            //}

            //context.Response.StatusCode = StatusCodes.Status401Unauthorized;


            //// await _next.Invoke(context);

        }
    }
}
